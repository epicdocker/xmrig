# XMRig

<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
* 2. [Requirements](#Requirements)
* 3. [Examples](#Examples)
	* 3.1. [Show Usage/Help](#ShowUsageHelp)
	* 3.2. [Start mining](#Startmining)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

Docker image for [XMRig](https://github.com/xmrig/xmrig) based on [Ubuntu](https://hub.docker.com/_/ubuntu) image.

##  1. <a name='Versions'></a>Versions

Of this project, there is always only one version `latest` and so that the version stays up to date, it is rebuilt weekly.

`latest` [Dockerfile](https://gitlab.com/epicdocker/xmrig/blob/master/Dockerfile)

##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-started

##  3. <a name='Examples'></a>Examples

###  3.1. <a name='ShowUsageHelp'></a>Show Usage/Help

```bash
docker run --rm -it registry.gitlab.com/epicdocker/xmrig:latest
```

###  3.2. <a name='Startmining'></a>Start mining

```bash
docker run -d \
  registry.gitlab.com/epicdocker/xmrig:latest \
    xmrig -a cryptonight \
      -o stratum+tcp://xmr.pool.minergate.com:45700 \
      -u ca2dh9r2tmyve3f \
      -p x
```
