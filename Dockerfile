FROM ubuntu:latest as build
ENV TZ=Europe/Berlin
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
 && apt-get -qq -y install git \
                           build-essential \
                           cmake \
                           libuv1-dev \
                           libssl-dev \
                           libhwloc-dev
RUN git clone https://github.com/xmrig/xmrig.git \
 && cd xmrig \
 && sed -i 's/kMinimumDonateLevel = 1/kMinimumDonateLevel = 0/g' src/donate.h \
 && mkdir build \
 && cmake -DCMAKE_BUILD_TYPE=Release . \
 && make

FROM ubuntu:latest
LABEL image.name="epicsoft_xmrig" \
      image.description="Docker image for xmrig" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2021-2022 epicsoft.de / Alexander Schwarz" \
      license="MIT"
ENV TZ=Europe/Berlin
RUN apt-get update \
 && apt-get -qq -y install ca-certificates \
                           hwloc \
 && rm -rf /var/lib/apt/lists/*
COPY --from=build /xmrig/xmrig /usr/local/bin/xmrig
CMD ["xmrig", "--help"]
